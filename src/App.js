import React from 'react';
import './App.css';

const base = ["Dragon", "Cruel", "Stone"]
const suffix = ["arm", "killer", "sword"]

const randomElement = array => array[Math.floor(Math.random() * array.length)];

function App() {
  const names = []
  for (let i = 0; i < 10; i++) {
    names.push(randomElement(base) + randomElement(suffix))
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Dwarven Names</h1>
        {names.map((name, index) => <p key={index}>{name}</p>)}
      </header>
    </div>
  );
}

export default App;
